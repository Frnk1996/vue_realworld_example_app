describe("My First Test", () => {
  beforeEach(() => {
    // Visit the login page URL
    cy.visit("http://localhost:8080/#/login");
  });

  it("should display login form", () => {
    // Check if the login form exists
    cy.get("form").should("exist");

    // Check if the username input field exists
    cy.get('input[placeholder="Email"]').should("exist");

    // Check if the password input field exists
    cy.get('input[placeholder="Password"]').should("exist");

    // Check if the submit button exists
    cy.get("button")
      .contains("Sign in")
      .should("exist");
  });

  it("should allow user to login with correct credentials", () => {
    // Fill in the username and password inputs
    cy.get('input[placeholder="Email"]').type("frank@mail.com");
    cy.get('input[placeholder="Password"]').type("test");

    // Submit the login form
    cy.get("button")
      .contains("Sign in")
      .click();

    // Wait for the login process to complete
    // cy.wait(2000); // Adjust this wait time as necessary

    // Check if the user is redirected to the dashboard or another expected page
    // cy.url().should("include", "/#/"); // Change '/dashboard' to the expected URL
  });
});
